//
//  MiLocalizer.swift
//  Track To School
//
//  Created by IOS DEVELOPER on 4/21/20.
//  Copyright © 2020 IOS DEVELOPER. All rights reserved.
//



import Foundation
import UIKit
extension UIApplication {
    
    class func isRTL() -> Bool{
        return UIApplication.shared.userInterfaceLayoutDirection == .rightToLeft
    }
}

class MyLocalizer: NSObject {
    
    class func DoTheMagic() {
        
        MethodSwizzleGivenClassName(cls: Bundle.self, originalSelector: #selector(Bundle.localizedString(forKey:value:table:)), overrideSelector: #selector(Bundle.specialLocalizedStringForKey(_:value:table:)))
        
        MethodSwizzleGivenClassName(cls: UIApplication.self, originalSelector: #selector(getter: UIApplication.userInterfaceLayoutDirection), overrideSelector: #selector(getter: UIApplication.cstm_userInterfaceLayoutDirection))
        
        MethodSwizzleGivenClassName(cls: UIView.self, originalSelector: #selector(getter: UIView.semanticContentAttribute), overrideSelector: #selector(getter: UIView.cstm_semanticContentAttribute))
       
    }
}

extension UIApplication {
    @objc var cstm_userInterfaceLayoutDirection : UIUserInterfaceLayoutDirection {
        
        get {
            var direction: UIUserInterfaceLayoutDirection!
            if MyLanguages.currentAppleLanguage() == "ar" {
                direction = .rightToLeft
            }
            else {
                
                direction = .leftToRight
            }
            
            return direction
        }
    }
}

extension UIView {
    @objc var cstm_semanticContentAttribute : UISemanticContentAttribute {
        
        get {
            var direction: UISemanticContentAttribute!
            if MyLanguages.currentAppleLanguage() == "ar" {
                direction = UISemanticContentAttribute.forceRightToLeft
            }
            else {
                
                direction = UISemanticContentAttribute.forceLeftToRight
            }
            
            return direction
        }
    }
}
extension Bundle {
    
    @objc func specialLocalizedStringForKey(_ key: String, value: String?, table tableName: String?) -> String {
        
        if self == Bundle.main {
            
            let currentLanguage = MyLanguages.currentAppleLanguage()
            var bundle = Bundle();
            
            if let _path = Bundle.main.path(forResource: MyLanguages.currentAppleLanguageFull(), ofType: "lproj") {
                
                bundle = Bundle(path: _path)!
            }
            else if let _path = Bundle.main.path(forResource: currentLanguage, ofType: "lproj") {
                
                bundle = Bundle(path: _path)!
                
            }
            else {
                
                let _path = Bundle.main.path(forResource: "Base", ofType: "lproj")!
                bundle = Bundle(path: _path)!
            }
            
            return (bundle.specialLocalizedStringForKey(key, value: value, table: tableName))
        }
        else {
            
            return (self.specialLocalizedStringForKey(key, value: value, table: tableName))
        }
    }
}
func disableMethodSwizzling() {
    
}


/// Exchange the implementation of two methods of the same Class
func MethodSwizzleGivenClassName(cls: AnyClass, originalSelector: Selector, overrideSelector: Selector) {
    
    let origMethod: Method = class_getInstanceMethod(cls, originalSelector)!;
    let overrideMethod: Method = class_getInstanceMethod(cls, overrideSelector)!;
    if (class_addMethod(cls, originalSelector, method_getImplementation(overrideMethod), method_getTypeEncoding(overrideMethod))) {
        class_replaceMethod(cls, overrideSelector, method_getImplementation(origMethod), method_getTypeEncoding(origMethod));
    }
    else {
        
        method_exchangeImplementations(origMethod, overrideMethod);
    }
}







//extensions




//MARK:- Localization  Tag 0 for Except
class MiViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if MyLanguages.currentAppleLanguage() == "ar" {
            
            flipForRTL(subviews: self.view.subviews)
        }
        else {
            
            flipForLTR(subviews: self.view.subviews)
        }
    }
}

extension UIViewController {
    
    //for Arabic
    func flipForRTL(subviews: [UIView]) {
        
        if subviews.count > 0 {
            
            for subView in subviews {
                
                //imageView flip
                if (subView is UIImageView) && subView.tag == 0 {
                    
                    let toRightArrow = subView as! UIImageView
                    
                    if let _img = toRightArrow.image {
                        
                        toRightArrow.image = UIImage(cgImage: _img.cgImage!, scale:_img.scale , orientation: UIImage.Orientation.upMirrored)
                    }
                }
                
                //Button flip
                if (subView is UIButton) && subView.tag == 1 {
                    
                    //let toRightArrow = subView as! UIButton   //vertical flip
                    //toRightArrow.transform = CGAffineTransform(rotationAngle: CGFloat.pi)
                    
                    let toRightArrow = subView as! UIButton    //horizontal flip
                    toRightArrow.transform = CGAffineTransform(scaleX: -1, y: 1)
                    
                }
                
                //Label flip
                if (subView is UILabel) && subView.tag == 0 {
                    
                    let toRightArrow = subView as! UILabel
                    toRightArrow.textAlignment = .right
                }
                //TextView flip
                if (subView is UITextView) && subView.tag == 0 {
                    
                    let toRightArrow = subView as! UITextView
                    toRightArrow.textAlignment = .right
                }
                
                //TextField flip
                if (subView is UITextField) && subView.tag == 0 {
                    
                    let toRightArrow = subView as! UITextField
                    toRightArrow.textAlignment = .right
                }
                
                flipForRTL(subviews: subView.subviews)
            }
        }
    }
    
    // For other languages(en,es...)
    func flipForLTR(subviews: [UIView]) {
        
        if subviews.count > 0 {
            
            for subView in subviews {
                
                //Label flip
                if (subView is UILabel) && subView.tag == 0 {
                    
                    let toRightArrow = subView as! UILabel
                    toRightArrow.textAlignment = .left
                }
                //TextView flip
                if (subView is UITextView) && subView.tag == 0 {
                    
                    let toRightArrow = subView as! UITextView
                    toRightArrow.textAlignment = .left
                }
                //TextField flip
                if (subView is UITextField) && subView.tag == 0 {
                    
                    let toRightArrow = subView as! UITextField
                    toRightArrow.textAlignment = .left
                }
                
                flipForLTR(subviews: subView.subviews)
            }
        }
    }
}



class MiTableViewCell: UITableViewCell {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        if MyLanguages.currentAppleLanguage() == "ar" {
            
            flipForRTL(subviews: self.contentView.subviews)
        }
        else {
            
            flipForLTR(subviews: self.contentView.subviews)
        }
    }
}


extension UITableViewCell {
    
    //for Arabic
    func flipForRTL(subviews: [UIView]) {
        
        if subviews.count > 0 {
            
            for subView in subviews {
                
                //imageView flip
                if (subView is UIImageView) && subView.tag == 0 {
                    
                    let toRightArrow = subView as! UIImageView
                    
                    if let _img = toRightArrow.image {
                        
                        toRightArrow.image = UIImage(cgImage: _img.cgImage!, scale:_img.scale , orientation: UIImage.Orientation.upMirrored)
                    }
                }
                
                //Button flip
                if (subView is UIButton) && subView.tag == 1 {
                    
                    let toRightArrow = subView as! UIButton
                    toRightArrow.transform = CGAffineTransform(scaleX: -1, y: 1) //CGAffineTransform(rotationAngle: CGFloat.pi)
                }
                
                //Label flip
                if (subView is UILabel) && subView.tag == 0 {
                    
                    let toRightArrow = subView as! UILabel
                    toRightArrow.textAlignment = .right
                }
                
                //TextField flip
                if (subView is UITextField) && subView.tag == 0 {
                    
                    let toRightArrow = subView as! UITextField
                    toRightArrow.textAlignment = .right
                }
                
                flipForRTL(subviews: subView.subviews)
            }
        }
    }
    
    // For other languages(en,es...)
    func flipForLTR(subviews: [UIView]) {
        
        if subviews.count > 0 {
            
            for subView in subviews {
                
                //Label flip
                if (subView is UILabel) && subView.tag == 0 {
                    
                    let toRightArrow = subView as! UILabel
                    toRightArrow.textAlignment = .left
                }
                //TextField flip
                if (subView is UITextField) && subView.tag == 0 {
                    
                    let toRightArrow = subView as! UITextField
                    toRightArrow.textAlignment = .left
                }
                
                flipForLTR(subviews: subView.subviews)
            }
        }
    }
}

//Localization  "hello".localized
extension String {
    
    var localized: String {
        return NSLocalizedString(self, comment: "")
    }
}



// MARK: Localizable
public protocol Localizable {
    var localizedX: String { get }
}

extension String: Localizable {
    public var localizedX: String {
        return NSLocalizedString(self, comment: "")
    }
}

// MARK: XIBLocalizable
public protocol XIBLocalizable {
    var xibLocKey: String? { get set }
}

extension UILabel: XIBLocalizable {
    @IBInspectable public var xibLocKey: String? {
        get { return nil }
        set(key) {
            text = key?.localizedX
        }
    }
}
extension UITextField: XIBLocalizable {
    @IBInspectable public var xibLocKey: String? {
        get { return nil }
        set(key) {
            placeholder = key?.localizedX
        }
    }
}
extension UIButton: XIBLocalizable {
    @IBInspectable public var xibLocKey: String? {
        get { return nil }
        set(key) {
            setTitle(key?.localizedX, for: .normal)
        }
    }
}
extension UITabBarItem: XIBLocalizable {
    @IBInspectable public var xibLocKey: String? {
        get { return nil }
        set(key) {
            title = key?.localizedX
        }
    }
}


/*
// MARK: Localizable
public protocol Localizable {
    var localizzed: String { get }
}

extension String: Localizable {
    public var localizzed: String {
        return NSLocalizedString(self, comment: "")
    }
}

// MARK: XIBLocalizable
public protocol XIBLocalizable {
    var xibLocKey: String? { get set }
}

extension UILabel: XIBLocalizable {
    @IBInspectable public var xibLocKey: String? {
        get { return nil }
        set(key) {
            text = key?.localized
        }
    }
}
extension UITextField: XIBLocalizable {
    @IBInspectable public var xibLocKey: String? {
        get { return nil }
        set(key) {
            placeholder = key?.localized
        }
    }
}

extension UIButton: XIBLocalizable {
    @IBInspectable public var xibLocKey: String? {
        get { return nil }
        set(key) {
            setTitle(key?.localized, for: .normal)
        }
    }
}
*/
 
