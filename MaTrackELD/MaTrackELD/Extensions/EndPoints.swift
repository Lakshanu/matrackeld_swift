//
//  EndPoints.swift
//  School App
//
//  Created by IOS DEVELOPER on 2/18/20.
//  Copyright © 2020 IOS DEVELOPER. All rights reserved.
//

import Foundation
import UIKit

class EndPoints {
    static let shared = EndPoints()
    private init() { }
    
    
    let ProductionURL = "https://api.matrack.live/gpstracking/matrackRouteApp/"
    let googleMpaAPIKey = "AIzaSyBXyLsYFQ7RTqX6Myea9j9XjDULpMbEoJo"
    //let geoCodeUrl = "http://45.33.22.249/reverse_geocode.php?"
    let geoCodeUrl = "http://sievamaps.com/reverse_geocode.php?"
    let loginAPI = "EnableLogin_app.php"
    let vehicleInfoAPI = "getVehicleInfo.php"
    let getSMSUser = "getSmsData.php"
    let addSMSUser = "saveSmsData.php"
    let routeAPI = "getRoute.php"
    let getgeofenceAPI = "getGeofenceData.php"
    let rateAPI = "saveRating.php"
    let getEmailAPI = "getEmailData.php"
    let saveEmailAPI = "saveEmailData.php"
    let notificationAPI = "getNotificationData.php"
    let changepasswordAPI = "changePassword.php"
    let changelanguageAPI = "changeLanguage.php"
    let emergencyAPI = "getEmergency.php"
    
    //Show Activity Indicator
    public var activityIndicatorTag: Int { return 999999 }
    public func showActivityIndicatorC(_ style: UIActivityIndicatorView.Style = .whiteLarge, location: CGPoint? = nil) {
        
        let sellf = UIApplication.shared.keyWindow!.rootViewController!
        
        if sellf.view.subviews.filter({ $0.tag == self.activityIndicatorTag}).count < 1 {
            UIApplication.shared.beginIgnoringInteractionEvents()
            let loc = location ?? sellf.view.center
            
            DispatchQueue.main.async(execute: {
                let activityIndicator = UIActivityIndicatorView(style: style)
                activityIndicator.color = UIColor.darkGray
                activityIndicator.tag = self.activityIndicatorTag
                activityIndicator.center = loc
                activityIndicator.hidesWhenStopped = true
                activityIndicator.startAnimating()
                sellf.view.addSubview(activityIndicator)
            })
        }
    }
    
    
    //Stops and removes an UIActivityIndicator in any UIViewController
    public func hideActivityIndicatorC() {
        let sellf = UIApplication.shared.keyWindow!.rootViewController!
        UIApplication.shared.endIgnoringInteractionEvents()
        DispatchQueue.main.async(execute: {
            if let activityIndicator = sellf.view.subviews.filter({ $0.tag == self.activityIndicatorTag}).first as? UIActivityIndicatorView {
                activityIndicator.stopAnimating()
                activityIndicator.removeFromSuperview()
            }
        })
    }
    
    
    
    
    
//    let reverseGeoURL = "http://45.33.22.249/reverse_geocode.php?"
//    let loginAPI = "login.php"
//    let routeAPI = "getRoute.php"
//    let getgeofenceAPI = "getGeofenceData.php"
//
//    let savegeofenceAPI = "https://api.matrack.live/gpstracking/matrackRouteApp/saveCustomerGeofence.php"
//    let deletegeofenceAPI = "https://api.matrack.live/gpstracking/matrackRouteApp/deleteGeofence.php"
}

