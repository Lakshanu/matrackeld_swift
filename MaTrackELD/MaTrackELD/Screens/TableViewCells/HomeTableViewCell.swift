//
//  HomeTableViewCell.swift
//  MaTrackELD
//
//  Created by IOS DEVELOPER on 8/18/20.
//  Copyright © 2020 IOS DEVELOPER. All rights reserved.
//

import UIKit

protocol LeftCellDelegate : class {
    func didPressLeftButton(_ tag: Int, sender: UIButton)
}

protocol RightCellDelegate : class {
    func didPressRightButton(_ tag: Int, sender: UIButton)
}

class HomeTableViewCell: UITableViewCell {

    @IBOutlet var leftImage: UIImageView!
    @IBOutlet var rightImage: UIImageView!
    @IBOutlet var leftlbl: UILabel!
    @IBOutlet var rightlbl: UILabel!
    @IBOutlet var leftBtn: UIButton!
    @IBOutlet var rightBtn: UIButton!
    
    var SelectLeftCell : LeftCellDelegate?
    var SelectRightCell : RightCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func leftBtnClicked(_ sender: UIButton) {
        SelectLeftCell?.didPressLeftButton(sender.tag, sender: sender)
    }
    
    @IBAction func rightBtnClicked(_ sender: UIButton) {
        SelectRightCell?.didPressRightButton(sender.tag, sender: sender)
    }
    
}
