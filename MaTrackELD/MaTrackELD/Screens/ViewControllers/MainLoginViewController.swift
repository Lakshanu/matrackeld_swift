//
//  MainLoginViewController.swift
//  MaTrackELD
//
//  Created by IOS DEVELOPER on 8/18/20.
//  Copyright © 2020 IOS DEVELOPER. All rights reserved.
//

import UIKit
import skpsmtpmessage

class MainLoginViewController: UIViewController, UITextFieldDelegate,URLSessionDelegate {
    
    
    @IBOutlet var username: UITextField!
    @IBOutlet var password: UITextField!
    @IBOutlet var offlineSwitch: UISwitch!
    @IBOutlet var signUpLbl: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
       
    }
    
    @IBAction func signInBtnClicked(_ sender: UIButton) {
    
        /* let message = SKPSMTPMessage()
               message.relayHost = "smtp.gmail.com"
               message.login = "testing01994@gmail.com"
               message.pass = "608561514"
               message.requiresAuth = true
               message.wantsSecure = true
               //message.relayPorts = [465]
               message.fromEmail = "testing01994@gmail.com"
               message.toEmail = "sandeep.s@sievanetworks2.com"
               message.subject = "Test email"
               let messagePart = [kSKPSMTPPartContentTypeKey: "text/plain; charset=UTF-8", kSKPSMTPPartMessageKey: "email body"]
               message.parts = [messagePart]
               message.delegate = self
               message.send()*/
              let dr = DiagnosticRequest();
              dr.eldIdentifier="LOG247"
              dr.eldRegistrationId="T2A0"
              callPingActionOfWebService(dr);
        
       // exampleSoapRequest();
    }
    
    @IBAction func connectBleCicked(_ sender: UIButton) {
        
    }
    
    @IBAction func disconnectBleClicked(_ sender: UIButton) {
        
    }
    
    @IBAction func offlineswitchAction(_ sender: UISwitch) {
    }
    
    /*private func exampleSoapRequest() {
       
              var is_SoapMessage: String = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:cgs=\"http://www.w3schools.com/webservices/\"><soapenv:Header/><soapenv:Body><cgs:CelsiusToFahrenheit><cgs:Celsius>20</cgs:Celsius></cgs:CelsiusToFahrenheit></soapenv:Body></soapenv:Envelope>"
        
        	 //let is_URL: String = "https://www.w3schools.com/webservices/tempconvert.asmx"
             let is_URL: String = "https://w3schools.com/xml/tempconvert.asmx?op=CelsiusToFahrenheit"
        let lobj_Request = NSMutableURLRequest(url: NSURL(string: is_URL)! as URL)
        let session = URLSession.shared
                    //let err: NSError?
            
        lobj_Request.httpMethod = "POST"
        lobj_Request.httpBody = is_SoapMessage.data(using: String.Encoding.utf8)
                    lobj_Request.addValue("www.w3schools.com", forHTTPHeaderField: "Host")
                    lobj_Request.addValue("text/xml; charset=utf-8", forHTTPHeaderField: "Content-Type")
                    lobj_Request.addValue(String(is_SoapMessage.count), forHTTPHeaderField: "Content-Length")
                    lobj_Request.addValue("http://www.w3schools.com/webservices/CelsiusToFahrenheit", forHTTPHeaderField: "SOAPAction")
              lobj_Request.addValue("application/soap+xml; charset=UTF-8", forHTTPHeaderField: "Content-Type")
            
        let task = session.dataTask(with: lobj_Request as URLRequest, completionHandler: {data, response, error -> Void in
                        print("Response: \(response)")
            let strData = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                        print("Body: \(strData)")
            
                        if error != nil
                        {
                            print("Error: " + error!.localizedDescription)
                        }
            
                    })
                    task.resume()
    }
*/

    func callPingActionOfWebService(_ diagnosticRequest: DiagnosticRequest?) {
    
        var soapMessage: String? = nil
        if let ELDIdentifier = diagnosticRequest?.eldIdentifier, let ELDRegistrationId = diagnosticRequest?.eldRegistrationId {
            soapMessage = """
                <!--?xml version="1.0" encoding= "UTF-8" ?-->\
                <v:Envelope xmlns:i="http://www.w3.org/2001/XMLSchema-instance" xmlns:d="http://www.w3.org/2001/XMLSchema" xmlns:c="http://www.w3.org/2003/05/soap-encoding" xmlns:v="http://www.w3.org/2003/05/soap-envelope">\
                <v:Header><n0:Action n0:mustUnderstand="1" xmlns:n0="http://www.w3.org/2005/08/addressing">http://www.fmcsa.dot.gov/schemas/FMCSA.ELD.Infrastructure/IELDSubmissionService/Ping</n0:Action></v:Header>\
                <v:Body>\
                <Ping xmlns="http://www.fmcsa.dot.gov/schemas/FMCSA.ELD.Infrastructure">\
                <data i:type="n1:DiagnosticRequest" xmlns:n1="http://www.fmcsa.dot.gov/schemas/FMCSA.ELD.Infrastructure">\
                <ELDIdentifier i:type="d:string">\(ELDIdentifier)</ELDIdentifier>\
                <ELDRegistrationId i:type="d:string">\(ELDRegistrationId)</ELDRegistrationId>\
                </data>\
                </Ping>\
                </v:Body>\
                </v:Envelope>
                """
        }
        print(soapMessage!)
        let url = URL(string: "https://eldws.fmcsa.dot.gov/ELDSubmissionService.svc?singleWsdl")
        var theRequest: NSMutableURLRequest? = nil
        if let url = url {
            theRequest = NSMutableURLRequest(url: url)
        }
        let msgLength = String(format: "%lu", UInt(soapMessage?.count ?? 0))
        theRequest?.addValue("application/soap+xml; charset=UTF-8", forHTTPHeaderField: "Content-Type")
        theRequest?.addValue(msgLength, forHTTPHeaderField: "Content-Length")
        theRequest?.httpMethod = "POST"
        theRequest?.httpBody = soapMessage?.data(using: .utf8)
        
        /***********************************************************************************/
    
       let configuration = URLSessionConfiguration.ephemeral
       let session = 	URLSession(configuration: configuration, delegate: self, delegateQueue: nil)
        
    		
        //let session = URLSession.shared
        let task = session.dataTask(with: url!, completionHandler: { data, response, error in
        
            let httpResponse = response as? HTTPURLResponse
                       if error == nil {
                           let statusCode = Int(httpResponse?.statusCode ?? 0)
                          print(statusCode)
                           if statusCode == 200 {
                            print("data ")
                               //BFLog("%@", String(utf8String: data?.bytes))
                             //  self.parseXML(data)
                           } else {
                               print("Not able to get the success response from server")
                             //  BFLog("Not able to get the success response from server")
                           }
                       } else {
                           if let error = error {
                               print("Received Error response: \(error)")
                           }
                           //BFLog("Received Error response: %@", error)
                       }
        })
       

    task.resume()
    
  
       
    
   
    func parseXML(_ xmlData: Data?) {
        print(xmlData)
    
        /*diagnosticResponse = DiagnosticResponse()
        var parser: XMLParser? = nil
        if let xmlData = xmlData {
            parser = XMLParser(data: xmlData)
        }
        parser?.delegate = self
        parser?.parse()
        print("Diagnostic Response: \(diagnosticResponse)")
        BFLog("Diagnostic Response: %@", diagnosticResponse)*/
    }
    
    func parser(_ parser: XMLParser, foundCharacters value: String) {
        // Keep track of the current element value
        //elementValue = value
    }
    
    func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
    
        /*if elementName == "Boradcast" {
            diagnosticResponse.broadcast = elementValue
        } else if elementName == "Detail" {
            diagnosticResponse.detail = elementValue
        } else if elementName == "Status" {
            let definedEnum = Enum()
            diagnosticResponse.status = definedEnum.getDiagnosticResponseStatusEnum(fromString: elementValue)
        }
    
        //Set the value null so next time even if not found characters the null value is set to the attribute
        elementValue = nil*/
    }
    
    func parser(_ parser: XMLParser, parseErrorOccurred parseError: Error) {
        print("\(parseError.localizedDescription)")
       // BFLog("%@", parseError.localizedDescription)
    }

    
    }
    
    /*fileprivate func SSLCertificateCreateTrustResult(_ serverTrust: SecTrust)->SecTrustResultType {
           let certificate: SecCertificate = SecTrustGetCertificateAtIndex(serverTrust, 0)!
           let remoteCertificateData = CFBridgingRetain(SecCertificateCopyData(certificate))!
           let certName = "matrackeld"
          
           let cerPath: String = Bundle.main.path(forResource: certName, ofType: "p12")!
           let localCertificateData = NSData(contentsOfFile:cerPath)!
       
           let certDataRef = localCertificateData as CFData
           let cert = (SecCertificateCreateWithData(nil, certDataRef))
           let certArrayRef = [cert] as CFArray
           SecTrustSetAnchorCertificates(serverTrust, certArrayRef)
           SecTrustSetAnchorCertificatesOnly(serverTrust, false)
           let trustResult: SecTrustResultType = SecTrustResultType.invalid
           return trustResult
       }*/
    
    func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        print("called url session "+challenge.protectionSpace.authenticationMethod)
        
       
               if challenge.protectionSpace.authenticationMethod == (NSURLAuthenticationMethodClientCertificate) {
                   print("called url session nsUrl authentication method server")
                
                if let localCertPath = Bundle.main.url(forResource: "matrackeld", withExtension: "p12"),
                           let localCertData = try?  Data(contentsOf: localCertPath)
                       {
                
                let identityAndTrust:IdentityAndTrust = extractIdentity(certData: localCertData as NSData, certPassword: "abc123")
                                   
                                  
                                       
                                       let urlCredential:URLCredential = URLCredential(
                                           identity: identityAndTrust.identityRef,
                                           certificates: identityAndTrust.certArray as [AnyObject],
                                           persistence: URLCredential.Persistence.forSession);
                                       
                                       completionHandler(URLSession.AuthChallengeDisposition.useCredential, urlCredential);
                                       
                                       return
                                   
                               }
                               
                               challenge.sender?.cancel(challenge)
                               completionHandler(URLSession.AuthChallengeDisposition.rejectProtectionSpace, nil)
                        
                  
               }
               else
               {
                
                   completionHandler(URLSession.AuthChallengeDisposition.cancelAuthenticationChallenge, nil);
               }
        }
           }
           
 
       
       func willSendRequest(for challenge: URLAuthenticationChallenge?, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
           print("called Send Request")
           
           //ELDConnectionHelper.urlSession(<#T##self: ELDConnectionHelper##ELDConnectionHelper#>)
       }
    
    
    
    public func extractIdentity(certData:NSData, certPassword:String) -> IdentityAndTrust {
        
        var identityAndTrust:IdentityAndTrust!
        var securityError:OSStatus = errSecSuccess
        
        var items: CFArray?
        let certOptions: Dictionary = [ kSecImportExportPassphrase as String : certPassword ];
         // import certificate to read its entries
        securityError = SecPKCS12Import(certData, certOptions as CFDictionary, &items);
        if securityError == errSecSuccess {
            
            let certItems:CFArray = (items as CFArray?)!;
            let certItemsArray:Array = certItems as Array
            let dict:AnyObject? = certItemsArray.first;
            
            if let certEntry:Dictionary = dict as? Dictionary<String, AnyObject> {
                
                // grab the identity
                let identityPointer:AnyObject? = certEntry["identity"];
                let secIdentityRef:SecIdentity = (identityPointer as! SecIdentity?)!;
                
                // grab the trust
                let trustPointer:AnyObject? = certEntry["trust"];
                let trustRef:SecTrust = trustPointer as! SecTrust;
                
                // grab the certificate chain
                var certRef: SecCertificate?
                SecIdentityCopyCertificate(secIdentityRef, &certRef);
                let certArray:NSMutableArray = NSMutableArray();
                certArray.add(certRef as SecCertificate?);
                
                identityAndTrust = IdentityAndTrust(identityRef: secIdentityRef, trust: trustRef, certArray: certArray);
            }
        }
        
        return identityAndTrust;
    
    
    
}
