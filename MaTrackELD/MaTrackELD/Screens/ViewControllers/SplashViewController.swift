//
//  SplashViewController.swift
//  MaTrackELD
//
//  Created by IOS DEVELOPER on 8/18/20.
//  Copyright © 2020 IOS DEVELOPER. All rights reserved.
//

import UIKit
import SQLite3

class SplashViewController: UIViewController {
    
    var db:DbHelper = DbHelper();
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        db.createTable();
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
       /* if(UserDefaults.standard.value(forKey: "already_loggedin") as? String != nil) {
           
            if(UserDefaults.standard.value(forKey: "already_loggedin") as? String == "true") {
                let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                let nextViewController = storyBoard.instantiateViewController(withIdentifier: "SWRevealViewController") as! SWRevealViewController
                nextViewController.modalPresentationStyle = .fullScreen
                UserDefaults.standard.set(0, forKey: "selectedTabIndex")
                self.present(nextViewController, animated:true, completion:nil)
            }
            
            else {
                let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                let nextViewController = storyBoard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                nextViewController.modalPresentationStyle = .fullScreen
                self.present(nextViewController, animated:true, completion:nil)
            }
        }
            
        else {
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            nextViewController.modalPresentationStyle = .fullScreen
            self.present(nextViewController, animated:true, completion:nil)
        }
        */
        
        
            print("splash screen")
            let nextVC = self.storyboard?.instantiateViewController(withIdentifier: "MainLoginViewController") as! MainLoginViewController
            //DispatchQueue.main.async { self.navigationController?.pushViewController(nextVC, animated: true) }
        nextVC.modalPresentationStyle = .fullScreen
        self.present(nextVC, animated:true, completion:nil)
    
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
