//
//  HomeViewController.swift
//  MaTrackELD
//
//  Created by IOS DEVELOPER on 8/18/20.
//  Copyright © 2020 IOS DEVELOPER. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, LeftCellDelegate, RightCellDelegate {

    var imageArrayLeft = ["logs_3.png","dvir 2.png","suggestion_menu.png","udp.png","accidenticon.png","help_3.png"]
    var imageArrayRight = ["inspect.jpg","datatransfer.png","unapproved.png","message_alert.png","sett_3.jpg",""]
    var imageNameLeft = ["Logs","DVIR","Suggestions","Unidentified Profile","Accident Form","Help"]
    var imageNameRight = ["Roadside Inspection","Data Transfer","Unapproved Dates","Messages","Settings",""]
    
    @IBOutlet var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib.init(nibName: "HomeTableViewCell", bundle: nil), forCellReuseIdentifier: "HomeTableViewCell")
        tableView.rowHeight = 150
        tableView.separatorStyle = UITableViewCell.SeparatorStyle.singleLine
        tableView.separatorColor = .clear
        
    }
    
    func didPressLeftButton(_ tag: Int, sender: UIButton) {
        print("left tag", tag)
    }
       
    func didPressRightButton(_ tag: Int, sender: UIButton) {
        print("right tag", tag)
    }
    

}



//Table view
extension HomeViewController {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        for view in tableView.subviews {
//            if view is UIScrollView {
//                (view as? UIScrollView)!.delaysContentTouches = false
//                break } }
        return 6
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HomeTableViewCell", for: indexPath) as! HomeTableViewCell
        cell.SelectRightCell = self
        cell.SelectLeftCell = self
        cell.rightBtn.tag = indexPath.row
        cell.leftBtn.tag = indexPath.row
        cell.leftImage.image = UIImage(named: "\(imageArrayLeft[indexPath.row])")
        cell.rightImage.image = UIImage(named: "\(imageArrayRight[indexPath.row])")
        cell.leftlbl.text = "\(imageNameLeft[indexPath.row])"
        cell.rightlbl.text = "\(imageNameRight[indexPath.row])"
        cell.rightBtn.addTarget(self, action: #selector(self.rightbuttonClicked), for: .touchUpInside)
        cell.leftBtn.addTarget(self, action: #selector(self.leftbuttonClicked), for: .touchUpInside)
        return cell
        
    }
       
    
    @objc func rightbuttonClicked(_ sender: UIButton) {
        print("rightbuttonClicked")
    }
    
    @objc func leftbuttonClicked(_ sender: UIButton) {
        print("leftbuttonClicked")
    }
  

}
