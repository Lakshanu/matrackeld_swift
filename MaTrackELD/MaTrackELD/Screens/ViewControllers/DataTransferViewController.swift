//
//  DataTransferViewController.swift
//  MaTrackELD
//
//  Created by IOS DEVELOPER on 8/18/20.
//  Copyright © 2020 IOS DEVELOPER. All rights reserved.
//

import UIKit

class DataTransferViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, CellDelegate {

    var lblArray = ["Inspect logs for last 15 days (including today)","Transfer driver's ELD output file to FMCSA server that will be made available to DOT inspection by FMCSA","Email eRods to fleet administer directly"]
    var lbl = ["","","Use data transfer option to transfer ELD output file to FMCSA server"]
    var btnArray = ["Begin Inspection","Data Transfer","Send Email"]
    
    @IBOutlet var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib.init(nibName: "DataTransferTableViewCell", bundle: nil), forCellReuseIdentifier: "DataTransferTableViewCell")
        tableView.rowHeight = 180
        tableView.separatorStyle = UITableViewCell.SeparatorStyle.singleLine
        tableView.separatorColor = .clear

    }
    
    func didPressButton(_ tag: Int, sender: UIButton) {
        print("Button tag", tag)
    }

}



//Table view
extension DataTransferViewController {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        for view in tableView.subviews {
            if view is UIScrollView {
                (view as? UIScrollView)!.delaysContentTouches = false
                break } }
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DataTransferTableViewCell", for: indexPath) as! DataTransferTableViewCell
        cell.SelectCell = self
        cell.btn.tag = indexPath.row
        cell.lblTxt.text = "\(lblArray[indexPath.row])"
        cell.lbl.text = "\(lbl[indexPath.row])"
        cell.btn.setTitle("\(btnArray[indexPath.row])", for: .normal)
        cell.btn.addTarget(self, action: #selector(self.buttonClicked), for: .touchUpInside)
        return cell
        
    }

    @objc func buttonClicked(_ sender: UIButton) {
        print("buttonClicked")
    }
    
}
