//
//  DbHelper.swift
//  MaTrackELD
//
//  Created by sieva networks on 8/19/20.
//  Copyright © 2020 IOS DEVELOPER. All rights reserved.
//

import Foundation
import SQLite3

class DbHelper
{
    init()
    {
        db = openDatabase()
        createTable()
    }

    let dbPath: String = "MatrackEld.sqlite"
    var db:OpaquePointer?

    func openDatabase() -> OpaquePointer?
    {
        let fileURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            .appendingPathComponent(dbPath)
        var db: OpaquePointer? = nil
        if sqlite3_open(fileURL.path, &db) != SQLITE_OK
        {
            print("error opening database")
            return nil
        }
        else
        {
            print("Successfully opened connection to database at \(dbPath)")
            return db
        }
    }
    
    func createTable() {
        let createTableString = "CREATE table IF NOT EXISTS userInfo(id INTEGER AUTO INCREMENT PRIMARY KEY,username TEXT,serveruserid INTEGER,authcode TEXT,currently_login INTEGER,language TEXT,timezone TEXT);"
        var createTableStatement: OpaquePointer? = nil
        if sqlite3_prepare_v2(db, createTableString, -1, &createTableStatement, nil) == SQLITE_OK
        {
            if sqlite3_step(createTableStatement) == SQLITE_DONE
            {
                print("user table created.")
            } else {
                print("user table could not be created.")
            }
        } else {
            print("CREATE TABLE statement could not be prepared.")
        }
        sqlite3_finalize(createTableStatement)
    }
}
