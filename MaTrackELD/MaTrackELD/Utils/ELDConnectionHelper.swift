//
//  EldWebConnectionHelper.swift
//  MaTrackELD
//
//  Created by sieva networks on 8/20/20.
//  Copyright © 2020 IOS DEVELOPER. All rights reserved.
//

import Foundation

/*public struct IdentityAndTrust {
    public var identityRef:SecIdentity
    public var trust:SecTrust
    public var certArray:NSArray
}
*/
/*public func extractIdentity(certData:NSData, certPassword:String) -> IdentityAndTrust {
    
    var identityAndTrust:IdentityAndTrust!
    var securityError:OSStatus = errSecSuccess
    
    var items: CFArray?
    let certOptions: Dictionary = [ kSecImportExportPassphrase as String : certPassword ];
     // import certificate to read its entries
    securityError = SecPKCS12Import(certData, certOptions as CFDictionary, &items);
    if securityError == errSecSuccess {
        
        let certItems:CFArray = (items as CFArray?)!;
        let certItemsArray:Array = certItems as Array
        let dict:AnyObject? = certItemsArray.first;
        
        if let certEntry:Dictionary = dict as? Dictionary<String, AnyObject> {
            
            // grab the identity
            let identityPointer:AnyObject? = certEntry["identity"];
            let secIdentityRef:SecIdentity = (identityPointer as! SecIdentity?)!;
            
            // grab the trust
            let trustPointer:AnyObject? = certEntry["trust"];
            let trustRef:SecTrust = trustPointer as! SecTrust;
            
            // grab the certificate chain
            var certRef: SecCertificate?
            SecIdentityCopyCertificate(secIdentityRef, &certRef);
            let certArray:NSMutableArray = NSMutableArray();
            certArray.add(certRef as SecCertificate?);
            
            identityAndTrust = IdentityAndTrust(identityRef: secIdentityRef, trust: trustRef, certArray: certArray);
        }
    }
    
    return identityAndTrust;
}
*/


/*public class ELDConnectionHelper : NSObject, URLSessionDelegate {
    
    public func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        if let localCertPath = Bundle.main.url(forResource:"matrackeld", withExtension: "p12"),
            let localCertData = try?  Data(contentsOf: localCertPath)
        {
            
            let identityAndTrust:IdentityAndTrust = extractIdentity(certData: localCertData as NSData, certPassword: "abc123")
            
            if challenge.protectionSpace.authenticationMethod == NSURLAuthenticationMethodClientCertificate {
                
                let urlCredential:URLCredential = URLCredential(
                    identity: identityAndTrust.identityRef,
                    certificates: identityAndTrust.certArray as [AnyObject],
                    persistence: URLCredential.Persistence.forSession);
                
                completionHandler(URLSession.AuthChallengeDisposition.useCredential, urlCredential);
                
                return
            }
        }
        
        challenge.sender?.cancel(challenge)
        completionHandler(URLSession.AuthChallengeDisposition.rejectProtectionSpace, nil)
    }*/
    
    
    
    /*fileprivate func SSLCertificateCreateTrustResult(_ serverTrust: SecTrust)->SecTrustResultType {
        let certificate: SecCertificate = SecTrustGetCertificateAtIndex(serverTrust, 0)!
        let remoteCertificateData = CFBridgingRetain(SecCertificateCopyData(certificate))!
        var certName = "matrackeld"
       
        let cerPath: String = Bundle.main.path(forResource: certName, ofType: "p12")!
        let localCertificateData = NSData(contentsOfFile:cerPath)!
    
        let certDataRef = localCertificateData as CFData
        let cert = (SecCertificateCreateWithData(nil, certDataRef))
        let certArrayRef = [cert] as CFArray
        SecTrustSetAnchorCertificates(serverTrust, certArrayRef)
        SecTrustSetAnchorCertificatesOnly(serverTrust, false)
        let trustResult: SecTrustResultType = SecTrustResultType.invalid
        return trustResult
    }
    
    public func extractIdentity(certData:NSData, certPassword:String) -> IdentityAndTrust {
            
            var identityAndTrust:IdentityAndTrust!
            var securityError:OSStatus = errSecSuccess
            
            var items: CFArray?
            let certOptions: Dictionary = [ kSecImportExportPassphrase as String : certPassword ];
             // import certificate to read its entries
            securityError = SecPKCS12Import(certData, certOptions as CFDictionary, &items);
            if securityError == errSecSuccess {
                
                let certItems:CFArray = (items as CFArray?)!;
                let certItemsArray:Array = certItems as Array
                let dict:AnyObject? = certItemsArray.first;
                
                if let certEntry:Dictionary = dict as? Dictionary<String, AnyObject> {
                    
                    // grab the identity
                    let identityPointer:AnyObject? = certEntry["identity"];
                    let secIdentityRef:SecIdentity = (identityPointer as! SecIdentity?)!;
                    
                    // grab the trust
                    let trustPointer:AnyObject? = certEntry["trust"];
                    let trustRef:SecTrust = trustPointer as! SecTrust;
                    
                    // grab the certificate chain
                    var certRef: SecCertificate?
                    SecIdentityCopyCertificate(secIdentityRef, &certRef);
                    let certArray:NSMutableArray = NSMutableArray();
                    certArray.add(certRef as SecCertificate?);
                    
                    identityAndTrust = IdentityAndTrust(identityRef: secIdentityRef, trust: trustRef, certArray: certArray);
                }
            }
            
            return identityAndTrust;
        
        }
        
    
    
}*/
	

